package main

import (
	"github.com/ipfs/go-ipfs/plugin"
	"github.com/ipfs/go-ipfs/repo"
	"github.com/ipfs/go-ipfs/repo/fsrepo"
	gcs "gitlab.com/p_/go-ipfs-gcs-datastore"
)

var Plugins = []plugin.Plugin{
	&GCSPlugin{},
}

type GCSPluginConfig struct {
	cfg gcs.Config
}

func (c *GCSPluginConfig) DiskSpec() fsrepo.DiskSpec {
	return fsrepo.DiskSpec{
		"bucket": c.cfg.Bucket,
	}
}

type GCSPlugin struct{}

func (p GCSPlugin) Name() string {
	return "gcs-datastore-plugin"
}

func (p GCSPlugin) Version() string {
	return "3.1.4"
}

func (p GCSPlugin) Init() error {
	return nil
}

func (p GCSPlugin) DatastoreTypeName() string {
	return "gcsds"
}

func (p GCSPlugin) DatastoreConfigParser() fsrepo.ConfigFromMap {
	return func(m map[string]interface{}) (fsrepo.DatastoreConfig, error) {
		return &GCSPluginConfig{
			cfg: gcs.Config{
				Prefix: "test01/",
				Bucket: "go-ipfs-gcs-datastore-01",
			}}, nil
	}
}

func (c *GCSPluginConfig) Create(path string) (repo.Datastore, error) {
	return gcs.NewDatastore(c.cfg)
}
