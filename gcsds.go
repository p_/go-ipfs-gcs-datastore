package gcsds

import (
	"cloud.google.com/go/storage"
	"context"
	ds "github.com/ipfs/go-datastore"
	dsq "github.com/ipfs/go-datastore/query"
	"io/ioutil"
)

var ctx = context.Background()

type Datastore struct {
	client *storage.Client
	Prefix string
	Bucket *storage.BucketHandle
}

type Config struct {
	Prefix string
	Bucket string
}

func NewDatastore(conf Config) (*Datastore, error) {
	client, err := storage.NewClient(ctx)
	return &Datastore{
		client: client,
		Bucket: client.Bucket(conf.Bucket),
		Prefix: conf.Prefix,
	}, err
}

func (d *Datastore) Get(key ds.Key) (value []byte, err error) {
	reader, err := d.Bucket.Object(d.Prefix + key.String()).NewReader(ctx)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return nil, err
	}

	defer reader.Close()
	return data, nil
}

func (d *Datastore) Has(key ds.Key) (exists bool, err error) {
	_, err = d.Bucket.Object(d.Prefix + key.String()).Attrs(ctx)
	return err == nil, nil
}

func (d *Datastore) GetSize(key ds.Key) (size int, err error) {
	attrs, err := d.Bucket.Object(d.Prefix + key.String()).Attrs(ctx)
	return int(attrs.Size), err
}

func (d *Datastore) Query(q dsq.Query) (results dsq.Results, err error) {
	return nil, nil
}

func (d *Datastore) Put(key ds.Key, value []byte) (err error) {
	writer := d.Bucket.Object(d.Prefix + key.String()).NewWriter(ctx)
	writer.Write(value)
	return writer.Close()
}

func (d *Datastore) Delete(key ds.Key) (err error) {
	return d.Bucket.Object(d.Prefix + key.String()).Delete(ctx)
}

func (d *Datastore) Close() (err error) {
	return nil
}

func (d *Datastore) Batch() (ds.Batch, error) {
	return ds.NewBasicBatch(d), nil
}
