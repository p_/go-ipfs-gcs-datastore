module gitlab.com/p_/go-ipfs-gcs-datastore

go 1.12

require (

	cloud.google.com/go v0.40.0
	github.com/ipfs/go-datastore v0.0.5
	github.com/ipfs/go-ipfs v0.4.21
  github.com/mr-tron/base58 v1.1.2
)
